import { userApi } from "../Services/userApi";
import { userType } from "./Type/userType";
export const userIdAction = (id) => {
  return async (dispatch) => {
    const detail = await userApi(id);
    dispatch({ type: userType, payload: detail.data });
  };
};
