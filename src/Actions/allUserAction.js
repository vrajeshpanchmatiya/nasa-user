import { allUserApi } from "../Services/allUserApi";

export const allUserAction = async () => {
  const detail = await allUserApi();
  return detail.data;
};
