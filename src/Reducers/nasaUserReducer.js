import { userType } from "../Actions/Type/userType";
// initial value of State
const initialState = {
  data: [],
};
// Reducer for Store
export const nasaUserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case userType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
