import { Box, Button, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userIdAction } from "../Actions/userIdAction";
import { Link } from "react-router-dom";
import { allUserAction } from "../Actions/allUserAction";
const NasaUserForm = () => {
  const [id, setId] = useState(null);
  const [allAsteroid, setAllAsteroid] = useState([]);
  // useEffect for Asteroid Id
  useEffect(() => {
    const fetch = async () => {
      const data = await allUserAction();
      console.log(data.near_earth_objects);
      setAllAsteroid(data.near_earth_objects);
    };
    fetch();
  }, []);
  const dispatch = useDispatch();
  // Event of onchange
  const changeId = (e) => {
    setId(e.target.value);
  };
  //onClick event
  const handleAsteroidId = () => {
    dispatch(userIdAction(id));
  };
  //onClick Event
  const handleRandomAsteroid = () => {
    const id = allAsteroid.map(({ id }) => id);
    console.log(id);
    const randm = Math.ceil(Math.abs(Math.random() * id.length));
    console.log(id[randm]);
    dispatch(userIdAction(id[randm]));
  };
  return (
    <div>
      <Box>
        <h1>Asteroid Form</h1>
        <TextField
          name={id}
          label="Asteroid Id"
          color="primary"
          onChange={changeId}
          variant="outlined"
        />
        <Link to={{ pathname: "/NasaUserDetail" }}>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            onClick={handleAsteroidId}
          >
            Submit ID
          </Button>
        </Link>
        <Link to={{ pathname: "/NasaUserDetail" }}>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            onClick={handleRandomAsteroid}
          >
            Fetch Random Asteroid
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default NasaUserForm;
