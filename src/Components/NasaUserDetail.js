import { Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
const NasaUserDetail = () => {
  // UseSelector for fetching data
  const info = useSelector((state) => {
    return state.data;
  });
  //Asteroid User Detail
  return (
    <div>
      <Typography>
        <b>Name: </b>
        {info?.name}
      </Typography>
      <Typography>
        <b>URL: </b>
        {info?.nasa_jpl_url}
      </Typography>
      <Typography>
        <b>Potentially Hazardeous: </b>
        {info?.is_potentially_hazardous_asteroid ? "true" : "false"}
      </Typography>
    </div>
  );
};
export default NasaUserDetail;
